package lab;

public class Main {

	public static void main(String[] args) {
		Point p =  new Point(3,5);
		System.out.println("x = " + p.xCoord + " y = " + p.yCoord);
		
		Point p2 = new Point(6, 6);
		p2.yCoord = 7 ;
		Rectangle r2 = new Rectangle(6 ,8 , p2);
		r2.topLeft = p ;
		System.out.println(r2.area());
		
		System.out.println(r2.perimeter());
		Point[] corners = r2.corners();
		for (int i = 0 ; i < corners.length;i++) {
			System.out.println("x = " + corners[i].xCoord + " y = " + corners[i].yCoord);
			
			
		}
		Circle c = new Circle(8 , new Point(12,3));
		c.area();
	}
	

}
