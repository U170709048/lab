package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
	
	protected int height;
	
	public Cylinder(int radius , int height) {
		super(radius);
		this.height = height;
	}
	public double area() {
		return 2 * super.area() + 2 * Math.PI* radius * height ;
	}
	@Override
	public String toString() {
		return "Cylinder [height=" + height + super.toString() + "]";
	}
	public int volume() {
		return (int) (area() * height);
		
	}
}
