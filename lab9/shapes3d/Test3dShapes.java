package shapes3d;

public class Test3dShapes {

	public static void main(String[] args) {
		Cylinder cyl = new Cylinder(5 , 4);
		double area = cyl.area();
		System.out.println(area);
		Cube cube = new Cube(5);
		area = cube.area();
		System.out.println(area);
	}
}
