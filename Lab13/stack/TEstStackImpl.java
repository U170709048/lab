package stack;

public class TEstStackImpl {
	public static void main(String[] args) {
		Stack stack = new StackArrayImp();
		
		stack.push("A");				
		stack.push("B");
		stack.push("C");
		stack.push("D");
		while (!stack.empty()) {
			System.out.println(stack.pop());}
		}
}
